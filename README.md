# acs-exasol

Exasol docker image with extras:

- increase disk size
- support for container restarts e.g. changing ip
- change nameserver settings
- ... and more

The entrypoint understands additional keywords:

* bash - skips any modification steps and directly starts ```/bin/bash```
* init-check - modifications usually run by an init container e.g. with a k8s statefulset
* original - skips any modifications and falls through to original entrypoint. Append arguments for orignal entrypoint script.

Anything other keyword starts the modification steps and eventually the original entrypoint.

## examples

#### Build the image

```bash
docker-compose build
```

#### k8s

Save the image:
```bash
docker save -o acs-exasol alligatorcompany/acs-exasol:7.1.24.1
```

Start cluster, import image and deploy with kustomize:
``` bash
k3d cluster create acsdev
k3d image import acs-exasol -c acsdev
CTX=k3d-acsdev
kubectl --context=$CTX create namespace dev
kustomize build k8s/environment/dev |  kubectl --context=$CTX apply -f -
```

#### restart with docker-compose

Initial start:
```bash
docker-compose up -d
```

Shut the service down:
```bash
docker-compose down
```
This may lead to the reassignment of the network address
and this must then be changed in the Exasol config.

Changing the network address and restart of the service is done with:
```bash
docker-compose run exasol init-check
docker-compose up -d
```

