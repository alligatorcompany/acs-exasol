#!/bin/bash
if test -f /exa/etc/EXAConf ; then
    echo "Current setting: $(cat /exa/etc/EXAConf | grep PrivateNet)"
    # grep "inet " suppresses the output of inet6 addresses
    NEWIP=$(ip addr show dev eth0 | grep "inet "  | cut -d\   -f 6 | cut -d '/' -f 1)
    echo "Local IP: $NEWIP"
    exaconf modify-node -n 11 -p $NEWIP/32
    echo "Modified setting: $(cat /exa/etc/EXAConf | grep PrivateNet)"
else
    echo "/exa/etc/EXAConf not found - assuming first start - nothing to do"
fi
