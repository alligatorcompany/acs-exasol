#!/bin/bash

EXA_VERSION=$(exaconf version)
exa_config="/exa/etc/EXAConf"
exa_localconf="/usr/opt/EXASuite-7/EXAClusterOS-${EXA_VERSION}/bin/exalocalconf"
exa_entrypoint="/usr/opt/EXASuite-7/EXAClusterOS-${EXA_VERSION}/docker/entrypoint.sh"

# cgroup tips from https://stackoverflow.com/questions/20096632/limit-memory-on-a-docker-container-doesnt-work
## exa_dbram = 9223372036854775807 is equivalent to "no memory limit set"
exa_dbram=$(cat /sys/fs/cgroup/memory/memory.limit_in_bytes)
if [[ -z "$exa_dbram" || "$exa_dbram" == 9223372036854771712 ]]; then
    echo "Could not gather memory ${exa_dbram}. Setting 5GB default"
    exa_dbram=5368709120
else
    exa_dbram="${exa_dbram}"
fi
dbram_mb=$(echo "scale=0; $exa_dbram / 1024^2" | bc)
echo "Gathered RAM $exa_dbram / $dbram_mb."
dwad_client print-setup DB1 > /tmp/db1.cfg
sed -i "/OVERALL_DBRAM/c\OVERALL_DBRAM:            $dbram_mb" /tmp/db1.cfg

nameserver=$(/work/kubectl -n kube-system get service kube-dns --output=jsonpath={.spec.clusterIP})
namespace=$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace) || namespace="default"
searchdomains="${namespace}.svc.cluster.local svc.cluster.local cluster.local"
echo "Searchdomains: ${searchdomains}"
nameserver=${nameserver:="8.8.8.8"}
echo "Nameserver: ${nameserver}"
echo "nameserver ${nameserver}" > /etc/resolv.conf
echo "search ${searchdomains}" >> /etc/resolv.conf

(
    set -e
    echo "Set nameserver to ${nameserver}"
    sed -i \
        -e "s/NameServers = .*$/NameServers = ${nameserver}/" \
        -e 's/Checksum = .*$/Checksum = COMMIT/' \
        "${exa_config}"

    if [[ $(grep "EnableAuditing" /exa/etc/EXAConf | wc -l) -eq 0 ]]; then
        sed -i -e '/MemSize = .* GiB/a \\tEnableAuditing = yes' "${exa_config}"
    fi
    if [[ $(grep "SearchDomains" /exa/etc/EXAConf | wc -l) -eq 0 ]]; then
        sed -i -e "/NameServers = .*/a SearchDomains = ${searchdomains}" "${exa_config}"
    else
        sed -i -e "s/SearchDomains = .*/SearchDomains = ${searchdomains}/" "${exa_config}"
    fi
    sed -i -e "s/MemSize = .*/MemSize = ${exa_dbram}/" "${exa_config}"

    if [[ $(grep "options" /etc/resolv.conf | wc -l) -eq 0 ]]; then
        sed -i -e '/search .*/a options ndots:5' "/etc/resolv.conf"
    fi
)
