#!/bin/bash

cp /work/exasol-kafka-connector-extension-*.jar /exa/data/bucketfs/bfsdefault/default/
chown exadefusr:exausers /exa/data/bucketfs/bfsdefault/default/exasol-kafka-connector-extension-*.jar
chmod a=rx /exa/data/bucketfs/bfsdefault/default/exasol-kafka-connector-extension-*.jar

 