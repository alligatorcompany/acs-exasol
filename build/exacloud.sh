#!/bin/bash
TARGETDIR=/exa/data/bucketfs/bfsdefault/default/
cp /work/exasol-cloud-storage-extension*.jar $TARGETDIR/
chown -R exadefusr:exausers $TARGETDIR/exasol-cloud-storage-extension*.jar
chmod a=rx $TARGETDIR/exasol-cloud-storage-extension*.jar
