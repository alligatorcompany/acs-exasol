#!/bin/bash

# use extended du features. 
# Smallest calculated value is 1G. Exasol default for a docker image is 6G. And acs-exasol docker images defaults to 20G.
curr_size=$(du --block-size=G --apparent-size /exa/data/storage/dev.1 | cut -dG -f1)

# was: curr_size=$(ls -lh /exa/data/storage/dev.1 | awk '{print substr($5, 1, length($5)-1)}' | sed "s/\..*//")

    if [[ $curr_size -lt ${DBSIZE} ]]; then
        echo "Expanding the storage to ${DBSIZE}GiB"
        truncate --size=${DBSIZE}G /exa/data/storage/dev.1 && cshdd --enlarge --node-id 11 -h /exa/data/storage/dev.1
    else
        echo "No need to expand storage!"
    fi
