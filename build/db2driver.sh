#!/bin/bash
TARGETDIR=/exa/data/bucketfs/bfsdefault/.dest/default/drivers/jdbc/db2
mkdir -p $TARGETDIR
cp /work/db2jcc4.jar $TARGETDIR/db2jcc4.jar
cp /work/db2settings.cfg $TARGETDIR/settings.cfg
chown -R exadefusr:exausers $TARGETDIR
chmod -R a=rx $TARGETDIR
