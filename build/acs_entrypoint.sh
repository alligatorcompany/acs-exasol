#!/bin/bash

EXA_VERSION=$(exaconf version)
EXA_LOCALCONF="/usr/opt/EXASuite-7/EXAClusterOS-${EXA_VERSION}/bin/exalocalconf"
EXA_ENTRYPOINT="/usr/opt/EXASuite-7/EXAClusterOS-${EXA_VERSION}/docker/entrypoint.sh"
exa_config="/exa/etc/EXAConf"

set -e

case "$1" in
"bash")
    export PATH="/usr/opt/EXASuite-7/EXASolution-${EXA_VERSION}/bin/Console:/usr/opt/EXASuite-7/EXAClusterOS-${EXA_VERSION}/sbin:/usr/opt/EXASuite-7/EXARuntime-${EXA_VERSION}/sbin:/usr/opt/EXASuite-7/EXAClusterOS-${EXA_VERSION}/bin:/usr/opt/EXASuite-7/EXARuntime-${EXA_VERSION}/bin:$PATH"
    exec "/bin/bash" "${@:2}"
;;
"init-check")
    export PATH="/usr/opt/EXASuite-7/EXASolution-${EXA_VERSION}/bin/Console:/usr/opt/EXASuite-7/EXAClusterOS-${EXA_VERSION}/sbin:/usr/opt/EXASuite-7/EXARuntime-${EXA_VERSION}/sbin:/usr/opt/EXASuite-7/EXAClusterOS-${EXA_VERSION}/bin:/usr/opt/EXASuite-7/EXARuntime-${EXA_VERSION}/bin:$PATH"
    exec "/work/set_ip.sh"
;;
"original")
    exec "${EXA_ENTRYPOINT}" "${@}"
;;
*)
    /work/set_ip.sh

    (    
        if [ ! -f /exa/etc/custom_init_done ] ; then
            while [ ! -f /exa/etc/init_done ] ; do
                sleep 2
            done
            echo "Database up."
            /work/expand_storage.sh
            /work/kafka-connector.sh
            /work/db2driver.sh
            /work/sqlserverdriver.sh
            /work/exacloud.sh
            /work/modify_config.sh
            echo "done" > /exa/etc/custom_init_done
            dwad_client stop-wait DB1
            dwad_client setup DB1 /tmp/db1.cfg
            dwad_client start-wait DB1
            echo 'Database is ready to use!' >> /exa/logs/cored/exainit.log
        fi
    ) &

    # --> let Exasol entrypoint take over. default command is 'init-sc'.
    exec "${EXA_ENTRYPOINT}" "${@}"
    
;;
esac

echo "This should not happen"
exit -1
