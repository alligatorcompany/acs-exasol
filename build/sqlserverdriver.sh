#!/bin/bash
TARGETDIR=/exa/data/bucketfs/bfsdefault/.dest/default/drivers/jdbc/sqlserver
mkdir -p $TARGETDIR
cp /work/sqlserver-jdbc-9.2.0.jre8.jar $TARGETDIR/sqlserver-jdbc-9.2.0.jre8.jar
cp /work/sqlserversettings.cfg $TARGETDIR/settings.cfg
chown -R exadefusr:exausers $TARGETDIR
chmod -R a=rx $TARGETDIR
